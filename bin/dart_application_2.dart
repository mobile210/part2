import 'dart:io';

void main(List<String> args) {
  String? input = stdin.readLineSync()!;
  List<String> s1 = input.toLowerCase().split(' ');
  var map = {};

  for (var s in s1) {
    if (!map.containsKey(s)) {
      map[s] = 1;
    } else {
      map[s] += 1;
    }
  }

  print(map);
}
